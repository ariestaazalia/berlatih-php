<?php
    
    function ubah_huruf($str){
        $word= "abcdefghijklmnopqrstuvwxyz";        
        for($o= 0; $o<strlen($str); $o++){
            $curr = strpos($word, substr($str, $o,1), 1);
            $next= substr($word, $curr +1, 1);
            echo $next;
        }
    }

    // TEST CASES
    echo ubah_huruf('wow') . "<br>"; // xpx
    echo ubah_huruf('developer') . "<br>"; // efwfmpqfs
    echo ubah_huruf('laravel') . "<br>"; // mbsbwfm
    echo ubah_huruf('keren') . "<br>"; // lfsfo
    echo ubah_huruf('semangat') . "<br>"; // tfnbohbu

?>