<?php
    function tentukan_nilai($value)
    {
        if($value>=85 && $value<=100){
            echo "Sangat Baik";
        }elseif($value>=70 && $value<85){
            echo "Baik";
        }elseif($value>=60 && $value<70){
            echo "Cukup";
        }else{
            echo "Kurang";
        }
    }
    
    //TEST CASES
    echo tentukan_nilai(98) . "<br>"; //Sangat Baik
    echo tentukan_nilai(76). "<br>"; //Baik
    echo tentukan_nilai(67). "<br>"; //Cukup
    echo tentukan_nilai(43). "<br>"; //Kurang
?>